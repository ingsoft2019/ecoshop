package classes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ContainerTest {
    
    private Container cont;
    
    public ContainerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- CONTAINER TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        cont = new Container();
        cont.setName("Name");
        cont.setPrice(2);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetName() {
        System.out.println("getName");
        Container instance = cont;
        String expResult = "Name";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Name2";
        Container instance = cont;
        instance.setName(name);
        assertEquals(name, instance.getName());
    }

    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        Container instance = cont;
        double expResult = 2;
        double result = instance.getPrice();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetPrice() {
        System.out.println("setPrice");
        double price = 3;
        Container instance = cont;
        instance.setPrice(price);
        assertEquals(price, instance.getPrice(), 0.0);
    }
}
