package classes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SalePointTest {
    
    private SalePoint salePoint;
    
    public SalePointTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- SALE POINT TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        salePoint = new SalePoint();
        salePoint.setLatitude(1);
        salePoint.setLength(1);
        salePoint.setTitle("Title");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetLatitude() {
        System.out.println("getLatitude");
        SalePoint instance = salePoint;
        double expResult = 1;
        double result = instance.getLatitude();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetLatitude() {
        System.out.println("setLatitude");
        double latitude = 2;
        SalePoint instance = salePoint;
        instance.setLatitude(latitude);
        assertEquals(latitude, instance.getLatitude(), 0.0);
    }

    @Test
    public void testGetLength() {
        System.out.println("getLength");
        SalePoint instance = salePoint;
        double expResult = 1;
        double result = instance.getLength();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetLength() {
        System.out.println("setLength");
        double length = 2;
        SalePoint instance = salePoint;
        instance.setLength(length);
        assertEquals(length, instance.getLength(), 0.0);
    }

    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        SalePoint instance = salePoint;
        String expResult = "Title";
        String result = instance.getTitle();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetTitle() {
        System.out.println("setTitle");
        String title = "Title2";
        SalePoint instance = salePoint;
        instance.setTitle(title);
        assertEquals(title, instance.getTitle());
    }
    
}
