package classes;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SaleTest {
    
    private Sale sale;
    private List<Container> usedContainers;
    
    public SaleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- SALE TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        sale = new Sale();
        usedContainers = new ArrayList<Container>();
        
        Article art = new Article();
        Container cont = new Container();
        
        art.setCode("Code");
        art.setMaterial("Mat");
        art.setName("Name");
        art.setPrice(1);
        art.setRawMaterial("Raw");
        
        cont.setName("Cont");
        cont.setPrice(2);
        
        sale.setArticle(art);
        sale.setQuantity(2);
        sale.setUserdContainer(cont);
        
        usedContainers.add(cont);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArticle() {
        System.out.println("getArticle");
        Sale instance = sale;
        
        Article art = new Article();
        art.setCode("Code");
        art.setMaterial("Mat");
        art.setName("Name");
        art.setPrice(1);
        art.setRawMaterial("Raw");
        
        Article expResult = art;
        Article result = instance.getArticle();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetArticle() {
        System.out.println("setArticle");
        Article art = null;
        Sale instance = sale;
        instance.setArticle(art);
        assertEquals(null, instance.getArticle() );
    }

    @Test
    public void testGetQuantity() {
        System.out.println("getQuantity");
        Sale instance = sale;
        int expResult = 2;
        int result = instance.getQuantity();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetQuantity() {
        System.out.println("setQuantity");
        int quantity = 1;
        Sale instance = sale;
        instance.setQuantity(quantity);
        assertEquals(quantity, sale.getQuantity());
    }

    @Test
    public void testGetUsedContainer() {
        System.out.println("getUsedContainer");
        Sale instance = sale;
        Container result = instance.getUsedContainer();
        assertEquals(usedContainers.get(0), result);
    }

    @Test
    public void testSetUserdContainer() {
        System.out.println("setUserdContainer");
        Container container = null;
        Sale instance = new Sale();
        instance.setUserdContainer(container);
        assertNull(instance.getUsedContainer());
    }
    
}
