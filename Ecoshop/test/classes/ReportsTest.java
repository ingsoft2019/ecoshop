package classes;

import domain.Core;
import javax.swing.JPanel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReportsTest {
    
    private Reports rep;
    
    public ReportsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- REPORTS TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        rep = new Reports();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetCore() {
        System.out.println("setCore");
        Core core = new Core();
        Reports instance = new Reports();
        instance.setCore(core);
    }

    @Test
    public void testCreateProductsPanel() {
        System.out.println("createProductsPanel");
        Reports instance = new Reports();
        instance.setCore(new Core());
        JPanel panel = null;
        try {
            panel = instance.createProductsPanel();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull(panel);
    }

    @Test
    public void testCreateSavingsPanel() {
        System.out.println("createSavingsPanel");
        Reports instance = new Reports();
        instance.setCore(new Core() );
        JPanel panel = null;
        
        try {
            panel = instance.createSavingsPanel();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull(panel);
    }

    /**
     * Test of createPresalesPanel method, of class Reports.
     */
    @Test
    public void testCreatePresalesPanel() {
        System.out.println("createPresalesPanel");
        Reports instance = new Reports();
        instance.setCore(new Core() );
        JPanel panel = null;
        
        try {
            panel = instance.createPresalesPanel();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull(panel);
    }
    
}
