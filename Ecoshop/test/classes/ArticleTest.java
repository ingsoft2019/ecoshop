package classes;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArticleTest {
    
    private Article art;
    private List<Container> listContainer;
    
    public ArticleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- ARTICLE TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        art = new Article();
        art.setCode("CodeTest");
        art.setMaterial("MaterialTest");
        art.setName("TestName");
        art.setPrice(20.50);
        art.setRawMaterial("RawMaterialTest");
        
        Container cont = new Container();
        cont.setName("ContainerTest");
        cont.setPrice(2.2);
        listContainer = new ArrayList<Container>();
        listContainer.add(cont);
        
        art.addContainer(cont);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetMaterial() {
        System.out.println("getMaterial");
        Article instance = art;
        String expResult = "MaterialTest";
        assertEquals(expResult, instance.getMaterial());
    }

    @Test
    public void testSetMaterial() {
        System.out.println("setMaterial");
        String material = "MaterialTest2";
        String expResult = "MaterialTest2";
        Article instance = art;
        instance.setMaterial(material);
        assertEquals(expResult, instance.getMaterial());
    }

    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        Article instance = art;
        double expResult = 20.50;
        double result = instance.getPrice();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetPrice() {
        System.out.println("setPrice");
        double price = 10.70;
        Article instance = art;
        instance.setPrice(price);
        double expResult = 10.70;
        assertEquals(expResult, instance.getPrice(), 0.0);
    }

    @Test
    public void testGetCode() {
        System.out.println("getCode");
        Article instance = art;
        String expResult = "CodeTest";
        String result = instance.getCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetCode() {
        System.out.println("setCode");
        String code = "CodeTest2";
        Article instance = art;
        instance.setCode(code);
        assertEquals(code, instance.getCode());
    }

    @Test
    public void testGetName() {
        System.out.println("getName");
        Article instance = art;
        String expResult = "TestName";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "TestName2";
        Article instance = art;
        instance.setName(name);
        assertEquals(name, instance.getName());
    }

    @Test
    public void testGetRawMaterial() {
        System.out.println("getRawMaterial");
        Article instance = art;
        String expResult = "RawMaterialTest";
        String result = instance.getRawMaterial();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetRawMaterial() {
        System.out.println("setRawMaterial");
        String rawMaterial = "RawMaterialTest2";
        Article instance = art;
        instance.setRawMaterial(rawMaterial);
        assertEquals(rawMaterial, instance.getRawMaterial());
    }

    @Test
    public void testGetContainers() {
        System.out.println("getContainers");
        Article instance = art;
        List<Container> expResult = listContainer;
        List<Container> result = instance.getContainers();
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

    @Test
    public void testAddContainer() {
        System.out.println("addContainer");
        Article instance = art;

        Container container = new Container();
        container.setName("ContainerTest22");
        container.setPrice(4.4);
        
        instance.addContainer(container);
        this.listContainer.add(container);
        
        assertArrayEquals(listContainer.toArray(), instance.getContainers().toArray());
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Article instance = art;
        Article clon = new Article();
        clon.setCode("CodeTest");
        clon.setPrice(20.5);
        String expResult = clon.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        Article clon = new Article();
        clon.setCode( art.getCode() );
        Article instance = art;
        boolean expResult = true;
        boolean result = instance.equals(clon);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNoEquals() {
        System.out.println("noEquals");
        
        Article clon = new Article();
        clon.setCode("Code2");
        Article instance = art;
        boolean expResult = true;
        boolean result = instance.equals(clon);
        assertNotEquals(expResult, result);
    }
}
