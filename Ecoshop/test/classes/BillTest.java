package classes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class BillTest {
    
    private Bill bill;
    private Sale sale;
        
    public BillTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- BILL TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        bill = new Bill();
        sale = new Sale();
        Article art = new Article();
        Container cont = new Container();
        
        art.setCode("Code");
        art.setMaterial("Mat");
        art.setName("Name");
        art.setPrice(1);
        art.setRawMaterial("Raw");
        
        cont.setName("Cont");
        cont.setPrice(2);
        
        sale.setArticle(art);
        sale.setQuantity(2);
        sale.setUserdContainer(cont);
        
        bill.addSale(sale);
        bill.setCode("Code");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSales() {
        System.out.println("getSales");
        Bill instance = bill;
        List<Sale> expResult = new ArrayList<Sale>();
        expResult.add(sale);
        
        List<Sale> result = instance.getSales();
        assertEquals(expResult, result);
    }

    @Test
    public void testAddSale() {
        System.out.println("addSale");
        Sale sale = new Sale();
        
        Article ar = new Article();
        ar.setCode("Code2");
        ar.setMaterial("Mat2");
        ar.setName("Name2");
        ar.setPrice(2);
        ar.setRawMaterial("Raw2");
        
        sale.setArticle(ar);
        sale.setQuantity(1);
        
        Bill instance = bill;
        instance.addSale(sale);
    }

    @Test
    public void testSetCode() {
        System.out.println("setCode");
        String code = "CodeTest2";
        Bill instance = bill;
        instance.setCode(code);
        assertEquals(code, instance.getCode());
    }

    @Test
    public void testGetCode() {
        System.out.println("getCode");
        Bill instance = bill;
        String expResult = "Code";
        String result = instance.getCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDate() {
        System.out.println("getDate");
        Bill instance = bill;
        String expResult = new Date().toString();
        String result = instance.getDate().toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetDate() {
        System.out.println("setDate");
        Date date = null;
        Bill instance = new Bill();
        try {
            instance.setDate(date);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testSetIsPresale() {
        System.out.println("setIsPresale");
        Bill instance = new Bill();
        instance.setIsPresale(true);
        assertTrue(instance.isPresale());
    }

    @Test
    public void testIsPresale() {
        System.out.println("isPresale");
        Bill instance = new Bill();
        assertFalse(instance.isPresale());
    }
    
}
