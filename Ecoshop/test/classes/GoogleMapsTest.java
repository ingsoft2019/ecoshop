package classes;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class GoogleMapsTest {
    
    private GoogleMaps map;
    private List<SalePoint> salePoints;
    
    public GoogleMapsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- MAPS TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        map = new GoogleMaps();
        salePoints = new ArrayList<SalePoint>();
        
        SalePoint sp = new SalePoint();
        sp.setLatitude(1);
        sp.setLength(1);
        sp.setTitle("Test");
        
        salePoints.add(sp);
        map.setSalesPoints(salePoints);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSalesPoints() {
        System.out.println("getSalesPoints");
        GoogleMaps instance = map;
        List<SalePoint> expResult = salePoints;
        List<SalePoint> result = instance.getSalesPoints();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetSalesPoints() {
        System.out.println("setSalesPoints");
        GoogleMaps instance = map;
        instance.setSalesPoints(null);
        assertNull(instance.getSalesPoints());
    }
    
    @Test
    public void testGetFrame() {
        System.out.println("getFrame");
        GoogleMaps instance = new GoogleMaps();
        JFrame expResult = instance.getFrame();
        JFrame result = instance.getFrame();
        assertEquals(expResult, result);
    }
  
    @Test
    public void testCreateMap() {
        System.out.println("createMap");
        GoogleMaps instance = map;
        instance.createMap();     
        try {
            instance.getFrame();
        } catch (Exception e) {
            fail();
        }
    }    
}
