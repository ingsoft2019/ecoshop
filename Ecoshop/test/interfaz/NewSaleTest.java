package interfaz;

import domain.Core;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class NewSaleTest {
    
    public NewSaleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- NEW SALE TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetCore() {
        System.out.println("setCore");
        NewSale instance = new NewSale();
        try {
            instance.setCore(new Core());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testLoadInfo() {
        System.out.println("loadInfo");
        NewSale instance = new NewSale();
        instance.setCore(new Core());
        try {
            instance.loadInfo();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testDataBind() {
        System.out.println("dataBind");
        NewSale instance = new NewSale();
        instance.setCore(new Core());
        try { 
            instance.dataBind();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        try {
            NewSale.main(args);
        } catch (Exception e) {
            fail();
        }
    }
    
}
