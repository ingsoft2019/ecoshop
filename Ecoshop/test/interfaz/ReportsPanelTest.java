package interfaz;

import javax.swing.JPanel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReportsPanelTest {
    
    public ReportsPanelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- REPORTS PANEL TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        try {
            ReportsPanel.main(args);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testSetPanel() {
        System.out.println("setPanel");
        JPanel panel = new JPanel();
        ReportsPanel instance = new ReportsPanel();
        try {
            instance.setPanel(panel);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
}
