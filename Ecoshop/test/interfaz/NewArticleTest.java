package interfaz;

import domain.Core;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class NewArticleTest {
    
    public NewArticleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- NEW ARTICLE TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetCore() {
        System.out.println("setCore");
        NewArticle instance = new NewArticle();
        try {
            instance.setCore(new Core());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testLoadInfo() {
        System.out.println("loadInfo");
        NewArticle instance = new NewArticle();
        instance.setCore(new Core());
        try {
            instance.loadInfo();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testDataBind() {
        System.out.println("dataBind");
        NewArticle instance = new NewArticle();
        instance.setCore(new Core());
        try {
            instance.dataBind();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        try {
            NewArticle.main(args);
        } catch (Exception e) {
            fail("No deberia dar error");
        }
    }
    
}
