/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.ClassesSuite;
import domain.DomainSuite;
import imgs.ImgsSuite;
import interfaz.InterfazSuite;
import jars.JarsSuite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import others.OthersSuite;

/**
 *
 * @author Sowl
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({JarsSuite.class, DomainSuite.class, InterfazSuite.class, OthersSuite.class, ClassesSuite.class, ImgsSuite.class})
public class RootSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
