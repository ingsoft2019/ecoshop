package others;

import classes.Article;
import classes.Bill;
import classes.Container;
import classes.Sale;
import domain.Core;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class TicketTemplateTest {
    
    public TicketTemplateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetBill() {
        System.out.println("setBill");
        Bill bill = new Bill();
        TicketTemplate instance = new TicketTemplate();
        try {
            instance.setBill(bill);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testSetCore() {
        System.out.println("setCore");
        Core core = new Core();
        TicketTemplate instance = new TicketTemplate();
        try {
            instance.setCore(core);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testLoadBillInfo() {
        System.out.println("loadBillInfo");
        Article art = new Article();
        art.setCode("CodeArticle");
        art.setMaterial("Mat");
        art.setName("Article");
        art.setPrice(2);
        art.setRawMaterial("Raw");
        
        Container cont = new Container();
        cont.setName("Cont");
        cont.setPrice(5);
        
        Sale sale = new Sale();
        sale.setArticle(art);
        sale.setQuantity(1);
        sale.setUserdContainer(cont);
        
        Bill bill = new Bill();
        bill.setCode("Code");
        bill.addSale(sale);
        
        TicketTemplate instance = new TicketTemplate();
        instance.setBill(bill);
        instance.setCore(new Core());
        
        try {
            instance.loadBillInfo();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
}
