package domain;

import classes.Article;
import classes.Bill;
import classes.Container;
import classes.Sale;
import classes.SalePoint;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoreTest {
    
    private Core core;
    private List<Article> articles;
    private List<SalePoint> salePoints;
    private List<String> materials;
    private List<String> rawMaterials;
    private List<Bill> bills;
    private List<Container> containers;
    
    public CoreTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("-- CORE TESTS --");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        core = new Core();
        salePoints = new ArrayList<SalePoint>();
        materials = new ArrayList<String>();
        rawMaterials = new ArrayList<String>();
        articles = new ArrayList<Article>();
        bills = new ArrayList<Bill>();
        containers = new ArrayList<Container>();
        
        core.getArticles().clear();
        core.getBills().clear();
        core.getContainerTableHeader().clear();
        core.getContainers().clear();
        core.getMaterials().clear();
        core.getRawMaterials().clear();
        core.getSalesPoints().clear();
        core.getSalesTableHeader().clear();
        
        Article art = new Article();
        art.setCode("Code");
        art.setName("Name");
        art.setPrice(1);
        art.setRawMaterial("Raw");
        art.setMaterial("Mat");
        
        core.addArticle(art);
        articles.add(art);
        
        SalePoint sp = new SalePoint();
        sp.setLatitude(1);
        sp.setLength(1);
        sp.setTitle("Test");
        
        core.addSalePoint(sp);
        salePoints.add(sp);
        
        String mat1 = "Material1";
        core.addMaterial(mat1);
        materials.add(mat1);
        
        String rawMat1 = "Raw Material 1";
        core.addRawMaterial(rawMat1);
        rawMaterials.add(rawMat1);
        
        Container cont = new Container();
        cont.setName("Cont");
        cont.setPrice(3);
        
        Sale sale = new Sale();
        sale.setArticle(art);
        sale.setQuantity(1);
        sale.setUserdContainer(cont);
        Bill bill = new Bill();
        bill.setCode("BillCode");
        bill.addSale(sale);
        
        core.addBill(bill);
        bills.add(bill);
        
        core.addContainer(cont);
        containers.add(cont);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSalesPoints() {
        System.out.println("getSalesPoints");
        Core instance = core;
        assertArrayEquals(salePoints.toArray(), instance.getSalesPoints().toArray());
    }

    @Test
    public void testGetMaterials() {
        System.out.println("getMaterials");
        Core instance = core;
        assertArrayEquals(materials.toArray(), instance.getMaterials().toArray());
    }

    @Test
    public void testGetArticles() {
        System.out.println("getArticles");
        Core instance = core;
        assertArrayEquals(articles.toArray(), instance.getArticles().toArray());
    }
    
    @Test
    public void testGetRawMaterials() {
        System.out.println("getRawMaterials");
        Core instance = core;
        assertArrayEquals(rawMaterials.toArray(), instance.getRawMaterials().toArray());
    }

    @Test
    public void testGetBills() {
        System.out.println("getBills");
        Core instance = core;
        assertArrayEquals(bills.toArray(), instance.getBills().toArray());
    }

    @Test
    public void testGetContainers() {
        System.out.println("getContainers");
        Core instance = core;
        assertArrayEquals(containers.toArray(), instance.getContainers().toArray());
    }

    @Test
    public void testAddSalePoint() {
        System.out.println("addSalePoint");
        SalePoint sp = new SalePoint();
        sp.setLatitude(2);
        sp.setLength(2);
        sp.setTitle("Title Test");
        
        Core instance = core;
        instance.addSalePoint(sp);
        salePoints.add(sp);
        
        assertArrayEquals(salePoints.toArray(), instance.getSalesPoints().toArray());
    }

    @Test
    public void testAddMaterial() {
        System.out.println("addMaterial");
        String material = "Mat2";
        Core instance = core;
        instance.addMaterial(material);
        materials.add(material);
        assertArrayEquals(materials.toArray(), instance.getMaterials().toArray());
    }

    @Test
    public void testAddArticle() {
        System.out.println("addArticle");
        Article article = new Article();
        article.setCode("Code2");
        article.setMaterial("Mat2");
        article.setName("ArtDos");
        article.setPrice(2);
        article.setRawMaterial("Raw2");
                
        Core instance = core;
        instance.addArticle(article);
        articles.add(article);
        
        assertArrayEquals(articles.toArray(), instance.getArticles().toArray());
    }

    @Test
    public void testAddRawMaterial() {
        System.out.println("addRawMaterial");
        String origin = "RawMatt";
        Core instance = core;
        instance.addRawMaterial(origin);
        rawMaterials.add(origin);
        assertArrayEquals(rawMaterials.toArray(), instance.getRawMaterials().toArray());
    }

    @Test
    public void testAddBill() {
        System.out.println("addBill");
        Bill bill = new Bill();
        Core instance = core;
        instance.addBill(bill);
        bills.add(bill);
        assertArrayEquals(bills.toArray(), instance.getBills().toArray());
    }

    @Test
    public void testAddContainer() {
        System.out.println("addContainer");
        Container container = new Container();
        Core instance = core;
        instance.addContainer(container);
        containers.add(container);
        assertArrayEquals(containers.toArray(), instance.getContainers().toArray());
    }

    @Test
    public void testSave() {
        System.out.println("save");
        Core instance = core;
        instance.save();
        try {
            assertNotNull( new FileInputStream("C:/Users/Public/Saved.txt") );
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testLoad() {
        System.out.println("load");
        Core instance = core;
        instance.load();
        try {
            assertNotNull( new FileInputStream("C:/Users/Public/Saved.txt") );
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testCreateSellPoints() {
        System.out.println("createSellPoints");
        Core instance = new Core();
        instance.createSellPoints();
        assertNotNull(instance.getSalesPoints());
    }

    @Test
    public void testCreateMaterials() {
        System.out.println("createMaterials");
        Core instance = new Core();
        instance.createMaterials();
        core.createMaterials();
        assertArrayEquals(core.getMaterials().toArray(), instance.getMaterials().toArray());
    }

    @Test
    public void testCreateRawMaterials() {
        System.out.println("createRawMaterials");
        Core instance = new Core();
        instance.createRawMaterials();
        core.createRawMaterials();
        assertArrayEquals(core.getRawMaterials().toArray(), instance.getRawMaterials().toArray());
    }
    
    @Test
    public void testCreateContainers() {
        System.out.println("createContainers");
        Core instance = new Core();
        instance.createContainers();
        assertNotNull(instance.getContainers());
    }

    @Test
    public void testGetSalesTableHeader() {
        System.out.println("getSalesTableHeader");
        Core instance = core;
        assertNotNull(instance.getSalesTableHeader());
    }

    @Test
    public void testGetContainerTableHeader() {
        System.out.println("getContainerTableHeader");
        Core instance = core;
        assertNotNull(instance.getContainerTableHeader());
    }

    @Test
    public void testLoadInfo() {
        System.out.println("loadInfo");
        Core instance = new Core();
        try {
            instance.loadInfo();
        } catch (Exception e) {
            fail();
        }
    }
    
}
