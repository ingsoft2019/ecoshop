package domain;

import classes.Article;
import classes.Bill;
import classes.Container;
import classes.SalePoint;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Core {

    private List<SalePoint> salesPoints; 
    private List<String> materials;
    private List<Article> articles;
    private List<String> rawMaterials;
    private List<Container> containers;
    private List<Bill> bills; 

    public Core() {
        salesPoints = new ArrayList<>();
        materials = new ArrayList<>();
        articles = new ArrayList<>();
        rawMaterials = new ArrayList<>();
        bills = new ArrayList<>();
        containers = new ArrayList<>();
        
        load();
    }
    
    //Getters
    public List<SalePoint> getSalesPoints() {
        return this.salesPoints;
    }
    
    public List<String> getMaterials() {
        return this.materials;
    }
    
    public List<Article> getArticles() {
        return this.articles;
    }
    
    public List<String> getRawMaterials() {
        return this.rawMaterials;
    }
    
    public List<Bill> getBills() {
        return this.bills;
    }
    
    public List<Container> getContainers() {
        return this.containers;
    }
    
    //Add
    public void addSalePoint(SalePoint sp) {
        this.salesPoints.add(sp);
    }
    
    public void addMaterial(String material) {
        this.materials.add(material);
    }
    
    public void addArticle(Article article) {
        this.articles.add(article);
    }    
    
    public void addRawMaterial(String origin) {
        this.rawMaterials.add(origin);
    }
    
    public void addBill(Bill bill) {
        this.bills.add(bill);
    }
    
    public void addContainer(Container container) {
        this.containers.add(container);
    }
        
    //Methods
    public void save() {
        FileOutputStream fileOut = null;
        ObjectOutputStream out = null;
        try {
            fileOut = new FileOutputStream("C:/Users/Public/Saved.txt");
            out = new ObjectOutputStream(fileOut);
            
            out.writeObject(salesPoints);
            out.writeObject(materials);
            out.writeObject(articles);
            out.writeObject(rawMaterials);
            out.writeObject(bills);
            
            out.close();
            fileOut.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }   
    }

    public void load() {
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        try {
            fileIn = new FileInputStream("C:/Users/Public/Saved.txt");
            in = new ObjectInputStream(fileIn);

            salesPoints = (ArrayList<SalePoint>) in.readObject();  
            materials = (ArrayList<String>) in.readObject();  
            articles = (ArrayList<Article>) in.readObject();  
            rawMaterials = (ArrayList<String>) in.readObject();
            bills = (ArrayList<Bill>) in.readObject();  

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (fileIn != null) {
                try  {
                    fileIn.close();
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }   
    }
    
    public void loadInfo() {
        createMaterials();
        createSellPoints();
        createRawMaterials();
        createContainers();
    }

    public void createSellPoints() {
        
        SalePoint sp1 = new SalePoint();
        sp1.setLatitude(-34.9038526);
        sp1.setLength(-56.1905803);
        sp1.setTitle("ORT");
        
        SalePoint sp2 = new SalePoint();
        sp2.setLatitude(-34.8735105);
        sp2.setLength(-56.1046529);
        sp2.setTitle("Rotondaro");
        
        SalePoint sp3 = new SalePoint();
        sp3.setLatitude(-34.9062357);
        sp3.setLength(-56.1861229);
        sp3.setTitle("IMM");
        
        SalePoint sp4 = new SalePoint();
        sp4.setLatitude(-34.8938251);
        sp4.setLength(-56.1663526);
        sp4.setTitle("Tres Cruces");
        
        this.salesPoints.clear();
        this.addSalePoint(sp1);
        this.addSalePoint(sp2);
        this.addSalePoint(sp3);
        this.addSalePoint(sp4);
    }
    
    public void createRawMaterials() {
        this.rawMaterials.clear();
        String recycled = "Reciclado";
        String newOrigin = "Organico";
        String sustOrigin = "Origen sustentable";
        String socResp = "Socialmente responsable";
        this.addRawMaterial(recycled);
        this.addRawMaterial(newOrigin);
        this.addRawMaterial(sustOrigin);
        this.addRawMaterial(socResp);
    }
    
    public void createMaterials() {
        this.materials.clear();
        String plastic = "Plastico";
        String wood = "Madera";
        String glass = "Vidrio";
        String paper = "Papel";
        this.addMaterial(plastic);
        this.addMaterial(wood);
        this.addMaterial(glass);
        this.addMaterial(paper);
    }

    public void createContainers() {
        this.containers.clear();
        Container plasticBag = new Container("Bolsa de Plastico", 4);
        Container paperBag = new Container("Bolsa de Papel", 3);
        Container glassBottle = new Container("Botella de Vidrio", 20);
        Container plasticBottle = new Container("Botella Retornable", 15);
        this.containers.add(plasticBag);
        this.containers.add(paperBag);
        this.containers.add(glassBottle);
        this.containers.add(plasticBottle);
    }

        
    public List<String> getSalesTableHeader() {
        List<String> headers = new ArrayList<>();
        headers.add("Article");
        headers.add("Cantidad");
        headers.add("Precio");
        headers.add("Envase");
        headers.add("Sub Total");
        return headers;
    }
    
    public List<String> getContainerTableHeader() {
        List<String> headers = new ArrayList<>();
        headers.add("Nombre");
        headers.add("Precio");
        return headers;
    }   
}
