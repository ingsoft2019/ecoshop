package others;

import classes.Bill;
import domain.Core;
import java.text.DecimalFormat;
import java.util.Calendar;
import javax.swing.table.DefaultTableModel;

public class TicketTemplate extends javax.swing.JPanel {

    private Bill bill;
    private Core core;
    private double iva;
    private DecimalFormat format;

    public TicketTemplate() {
        initComponents();
        iva = 22.0;
        format = new DecimalFormat("0.00");
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
    
    public void setCore(Core core) {
        this.core = core;
    }
    
    public void loadBillInfo() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(bill.getDate());
        txtDay.setText( String.valueOf( cal.get( Calendar.DAY_OF_MONTH ) ) );
        txtMonth.setText( String.valueOf( cal.get( Calendar.MONTH ) +  1 ) );
        txtYear.setText( String.valueOf( cal.get( Calendar.YEAR ) ) );
        
        DefaultTableModel model = new DefaultTableModel();
        
        core.getSalesTableHeader().forEach( (header) -> {
            model.addColumn(header);
        });
        
        this.bill.getSales().forEach( (sale) -> {

            double subTotal = sale.getArticle().getPrice() * sale.getQuantity();
            String[] rowSale = new String[ core.getSalesTableHeader().size() ];
            rowSale[0] = sale.getArticle().getName() + " - " + sale.getArticle().getCode();
            rowSale[1] = String.valueOf( sale.getQuantity() );
            rowSale[2] = "$ " + String.valueOf( sale.getArticle().getPrice() );
            rowSale[3] = sale.getUsedContainer() != null ? sale.getUsedContainer().getName() : "-";
            rowSale[4] = "$ " + subTotal;
          
            double total = Double.valueOf( txtSubtotal.getText().replace("$", "") );
            total += subTotal;
            txtSubtotal.setText("$ " + format.format(total));
            
            model.addRow(rowSale);

            if (sale.getUsedContainer() != null) {
                String[] rowContainer = new String[ core.getSalesTableHeader().size() ];
                rowContainer[0] = sale.getUsedContainer().getName();
                rowContainer[1] = "1";
                rowContainer[2] = "$ " + String.valueOf( sale.getUsedContainer().getPrice() );
                rowContainer[3] = "-";
                rowContainer[4] = "$ -" + String.valueOf( sale.getUsedContainer().getPrice() );
                
                double subSaving = sale.getUsedContainer().getPrice();
                double saving = Double.valueOf( txtSavings.getText().replace("$", "") );
                saving += subSaving;
                
                txtSavings.setText("$ " + format.format(saving));
                txtSubtotal.setText("$ " + format.format(total - saving));

                model.addRow(rowContainer);
            }

        });
        
        tblData.setModel(model);
        
        lblIva.setText( lblIva.getText().replace("#", String.valueOf(iva) ) );
        double subT = Double.parseDouble( txtSubtotal.getText().replace("$", "") );
        double calcIva = subT * (iva / 100);
        
        txtIva.setText( "$" + format.format(calcIva) );
        txtTotal.setText("$" + format.format(subT + calcIva) );
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        lblRUT = new javax.swing.JLabel();
        lblSerie = new javax.swing.JLabel();
        lblDirection = new javax.swing.JLabel();
        lblTel = new javax.swing.JLabel();
        lblBuyer = new javax.swing.JLabel();
        lblConsumption = new javax.swing.JLabel();
        txtBuyer = new javax.swing.JTextField();
        txtConsumption = new javax.swing.JTextField();
        lblDate = new javax.swing.JLabel();
        txtDay = new javax.swing.JTextField();
        txtMonth = new javax.swing.JTextField();
        txtYear = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        lblAddress = new javax.swing.JLabel();
        txtBuyerName = new javax.swing.JTextField();
        txtBuyerAddress = new javax.swing.JTextField();
        tblProducts = new javax.swing.JScrollPane();
        tblData = new javax.swing.JTable();
        lblSubtotal = new javax.swing.JLabel();
        txtSubtotal = new javax.swing.JTextField();
        lblIva = new javax.swing.JLabel();
        txtIva = new javax.swing.JTextField();
        lblTotal = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        lblSavings = new javax.swing.JLabel();
        txtSavings = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(520, 830));
        setMinimumSize(new java.awt.Dimension(520, 830));
        setPreferredSize(new java.awt.Dimension(520, 830));

        lblTitle.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(51, 204, 0));
        lblTitle.setText("EcoShop");

        lblRUT.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblRUT.setForeground(new java.awt.Color(0, 0, 0));
        lblRUT.setText("RUT: 125412874962");

        lblSerie.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblSerie.setForeground(new java.awt.Color(0, 0, 0));
        lblSerie.setText("Serie: A N 100.001");

        lblDirection.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblDirection.setForeground(new java.awt.Color(0, 0, 0));
        lblDirection.setText("Cuareim 1451 - Montevideo");

        lblTel.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblTel.setForeground(new java.awt.Color(0, 0, 0));
        lblTel.setText("Tel: 4523 2547 - 099 478 814");

        lblBuyer.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblBuyer.setForeground(new java.awt.Color(0, 0, 0));
        lblBuyer.setText("RUT COMPRADOR");

        lblConsumption.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblConsumption.setForeground(new java.awt.Color(0, 0, 0));
        lblConsumption.setText("CONSUMO FINAL");

        txtBuyer.setBackground(new java.awt.Color(255, 255, 255));
        txtBuyer.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtBuyer.setForeground(new java.awt.Color(0, 0, 0));
        txtBuyer.setEnabled(false);

        txtConsumption.setBackground(new java.awt.Color(255, 255, 255));
        txtConsumption.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtConsumption.setForeground(new java.awt.Color(0, 0, 0));
        txtConsumption.setEnabled(false);

        lblDate.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblDate.setForeground(new java.awt.Color(0, 0, 0));
        lblDate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDate.setText("FECHA");

        txtDay.setBackground(new java.awt.Color(255, 255, 255));
        txtDay.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtDay.setForeground(new java.awt.Color(0, 0, 0));
        txtDay.setEnabled(false);

        txtMonth.setBackground(new java.awt.Color(255, 255, 255));
        txtMonth.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtMonth.setForeground(new java.awt.Color(0, 0, 0));
        txtMonth.setEnabled(false);

        txtYear.setBackground(new java.awt.Color(255, 255, 255));
        txtYear.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtYear.setForeground(new java.awt.Color(0, 0, 0));
        txtYear.setEnabled(false);

        lblName.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblName.setForeground(new java.awt.Color(0, 0, 0));
        lblName.setText("NOMBRE");

        lblAddress.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblAddress.setForeground(new java.awt.Color(0, 0, 0));
        lblAddress.setText("DIRECCION");

        txtBuyerName.setBackground(new java.awt.Color(255, 255, 255));
        txtBuyerName.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtBuyerName.setForeground(new java.awt.Color(0, 0, 0));
        txtBuyerName.setEnabled(false);

        txtBuyerAddress.setBackground(new java.awt.Color(255, 255, 255));
        txtBuyerAddress.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtBuyerAddress.setForeground(new java.awt.Color(0, 0, 0));
        txtBuyerAddress.setEnabled(false);

        tblData.setBackground(new java.awt.Color(255, 255, 255));
        tblData.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        tblData.setForeground(new java.awt.Color(0, 0, 0));
        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblProducts.setViewportView(tblData);

        lblSubtotal.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblSubtotal.setForeground(new java.awt.Color(0, 0, 0));
        lblSubtotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSubtotal.setText("SUBTOTAL");

        txtSubtotal.setBackground(new java.awt.Color(255, 255, 255));
        txtSubtotal.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtSubtotal.setForeground(new java.awt.Color(0, 0, 0));
        txtSubtotal.setText("$ 0.0");
        txtSubtotal.setEnabled(false);

        lblIva.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblIva.setForeground(new java.awt.Color(0, 0, 0));
        lblIva.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIva.setText("IVA (#%)");

        txtIva.setBackground(new java.awt.Color(255, 255, 255));
        txtIva.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtIva.setForeground(new java.awt.Color(0, 0, 0));
        txtIva.setText("$ 0.0");
        txtIva.setEnabled(false);

        lblTotal.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(0, 0, 0));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTotal.setText("TOTAL");

        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtTotal.setForeground(new java.awt.Color(0, 0, 0));
        txtTotal.setText("$ 0.0");
        txtTotal.setEnabled(false);

        lblSavings.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        lblSavings.setForeground(new java.awt.Color(0, 0, 0));
        lblSavings.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSavings.setText("AHORRO");

        txtSavings.setBackground(new java.awt.Color(255, 255, 255));
        txtSavings.setFont(new java.awt.Font("Calibri Light", 0, 10)); // NOI18N
        txtSavings.setForeground(new java.awt.Color(0, 0, 0));
        txtSavings.setText("$ 0.0");
        txtSavings.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblDirection))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblRUT, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblTel)))
                        .addGap(6, 6, 6))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTitle)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tblProducts, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblBuyer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtBuyer)
                                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtBuyerName)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(lblConsumption, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtConsumption))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtDay, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(lblDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtMonth, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtBuyerAddress)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(369, 369, 369)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblIva, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtIva))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtSubtotal))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtTotal))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSavings, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtSavings)))))
                .addGap(18, 18, 18))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblRUT)
                            .addComponent(lblTel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDirection))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblBuyer)
                            .addComponent(lblConsumption)
                            .addComponent(lblDate))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBuyer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtConsumption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtBuyerName, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAddress)
                    .addComponent(txtBuyerAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tblProducts, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSubtotal)
                    .addComponent(txtSubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIva)
                    .addComponent(txtIva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotal)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSavings)
                    .addComponent(txtSavings, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(98, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblBuyer;
    private javax.swing.JLabel lblConsumption;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDirection;
    private javax.swing.JLabel lblIva;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblRUT;
    private javax.swing.JLabel lblSavings;
    private javax.swing.JLabel lblSerie;
    private javax.swing.JLabel lblSubtotal;
    private javax.swing.JLabel lblTel;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTable tblData;
    private javax.swing.JScrollPane tblProducts;
    private javax.swing.JTextField txtBuyer;
    private javax.swing.JTextField txtBuyerAddress;
    private javax.swing.JTextField txtBuyerName;
    private javax.swing.JTextField txtConsumption;
    private javax.swing.JTextField txtDay;
    private javax.swing.JTextField txtIva;
    private javax.swing.JTextField txtMonth;
    private javax.swing.JTextField txtSavings;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtYear;
    // End of variables declaration//GEN-END:variables
}
