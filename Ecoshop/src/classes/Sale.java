package classes;

import java.io.Serializable;

public class Sale implements Serializable {
    
    private static final long serialVersionUID = 1L;    
    private Article article;
    private int quantity;
    private Container usedContainer;
    
    public Sale() {
        article = new Article();
        quantity = 0;
        usedContainer = null;
    }
    
    public Article getArticle() {
        return this.article;
    }
    
    public void setArticle(Article art) {
        this.article = art;
    }
    
    public int getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public Container getUsedContainer() {
        return this.usedContainer;
    }
    
    public void setUserdContainer(Container container) {
        this.usedContainer = container;
    }
    
}
