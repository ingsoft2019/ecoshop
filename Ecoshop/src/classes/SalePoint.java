package classes;

import java.io.Serializable;

public class SalePoint implements Serializable {

    private static final long serialVersionUID = 1L;
    private double latitude;
    private double length;
    private String title;
    
    public SalePoint() {
    }
    
    //Getters & Setters
    public double getLatitude() {
        return this.latitude;
    }
    
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    
    public double getLength() {
        return this.length;
    }
    
    public void setLength(double length) {
        this.length = length;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
}
