package classes;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Bill implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<Sale> sales;
    private String code;
    private Date date;
    private boolean isPresale;

    
    public Bill() {
        sales = new ArrayList<>();
        code = "";
        date = Date.from(Instant.now());
        isPresale = false;
    }
    
    //Getters & Setters
    public List<Sale> getSales() {
        return this.sales;
    }
    
    public void addSale(Sale sale) {
        this.sales.add(sale);
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public void setIsPresale(boolean preSale) {
        this.isPresale = preSale;
    }
    
    public boolean isPresale() {
        return this.isPresale;
    }

}
