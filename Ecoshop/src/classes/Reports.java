
package classes;

import domain.Core;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class Reports {
    
    private Core core;
    private Map<String, Integer> dicProducts;
    private Map<String, Double> dicSavings;
    private Map<String, Integer> dicPresales;
    
    public Reports() {
        this.dicProducts = new HashMap<String, Integer>();
        this.dicSavings = new HashMap<String, Double>();
        this.dicPresales = new HashMap<String, Integer>();
    }
    
    public void setCore(Core core) {
        this.core = core;
    }
    
    private void loadDictionaryWithProducts() {
        
        this.dicProducts.clear();
        
        for (int i = 0; i < this.core.getBills().size(); i++ ) {
            
            Bill bill = this.core.getBills().get(i);
            
            bill.getSales().forEach( (sale) -> {
                
                String saleKey = sale.getArticle().getName();
                
                this.dicProducts.computeIfAbsent(saleKey, K -> sale.getQuantity());
                this.dicProducts.computeIfPresent(saleKey, (Key, Val) -> Val + sale.getQuantity() );
            });
        }
        
    }

    private void loadDictionaryWithSavings() {
        
        this.dicSavings.clear();
        
        for (int i = 0; i < this.core.getBills().size(); i++ ) {
            
            Bill bill = this.core.getBills().get(i);
            
            bill.getSales().forEach( (sale) -> {
                this.dicSavings.computeIfPresent("Total Ventas", (Key, Val) -> Val + (sale.getQuantity() * sale.getArticle().getPrice()));
                this.dicSavings.computeIfAbsent("Total Ventas", K -> (sale.getQuantity() * sale.getArticle().getPrice()));
                
                if (sale.getUsedContainer() != null) {
                    this.dicSavings.computeIfPresent("Total Ahorrado", (Key, Val) -> Val + sale.getUsedContainer().getPrice() );     
                    this.dicSavings.computeIfAbsent("Total Ahorrado", K -> sale.getUsedContainer().getPrice() );
                }
            });
        }
        
    }

    private void loadDictionaryPresales() {
        
        this.dicPresales.clear();
        
        for (int i = 0; i < this.core.getBills().size(); i++ ) {
            
            Bill bill = this.core.getBills().get(i);
            
            if (bill.isPresale()) {
                this.dicPresales.computeIfPresent("Preventa", (K, Val) -> Val + 1 );
                this.dicPresales.computeIfAbsent("Preventa", K -> 1);
            } else {
                this.dicPresales.computeIfPresent("Venta Normal", (K, Val) -> Val + 1 ); 
                this.dicPresales.computeIfAbsent("Venta Normal", K -> 1 );
            }
        }
    }


    //Products Report
    private PieDataset createProductsDataSet() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        loadDictionaryWithProducts();

        for ( Map.Entry<String, Integer> map : dicProducts.entrySet() ) {
            dataset.setValue( map.getKey() + "(" + map.getValue() + ")", map.getValue() );
        }
        
        return dataset;
    }

    private JFreeChart createProductsChart( PieDataset set) {
        return ChartFactory.createPieChart("Productos mas Vendidos", set, true, true, false);
    } 

    public JPanel createProductsPanel() {
        JFreeChart chart = createProductsChart(createProductsDataSet());
        return new ChartPanel(chart);
    }

    //Savings Report
    private PieDataset createSavingsDataSet() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        loadDictionaryWithSavings();

        for ( Map.Entry<String, Double> map : dicSavings.entrySet() ) {
            dataset.setValue(map.getKey() + "(" + map.getValue() + ")", map.getValue());
        }
        
        return dataset;
    }

    private JFreeChart createSavingsChart( PieDataset set) {
        return ChartFactory.createPieChart("Ahorros Generados", set, true, true, false);
    } 
    
    public JPanel createSavingsPanel() {
        JFreeChart chart = createSavingsChart(createSavingsDataSet());
        return new ChartPanel(chart);
    }

    //Presales Report
    private PieDataset createPresaleDataSet() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        loadDictionaryPresales();

        for ( Map.Entry<String, Integer> map : dicPresales.entrySet() ) {
            dataset.setValue(map.getKey() + "(" + map.getValue() + ")", map.getValue());
        }
        return dataset;
    }

    private JFreeChart createPresalesChart( PieDataset set) {
        return ChartFactory.createPieChart("Cantidad de Preventas realizadas", set, true, true, false);
    } 

    public JPanel createPresalesPanel() {
        JFreeChart chart = createPresalesChart(createPresaleDataSet());
        return new ChartPanel(chart);
    }   
}
