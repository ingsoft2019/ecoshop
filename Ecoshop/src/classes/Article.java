package classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Article implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private String material;
    private double price;
    private String code;
    private String name;
    private String rawMaterial;
    private List<Container> containers;
    
    public Article() {
        this.material = "";
        this.price = 0.0;
        this.code = "";
        this.name = "";
        this.rawMaterial = "";
        this.containers = new ArrayList<>();
    }
    
    //Getters & Setters    
    public String getMaterial() {
        return this.material;
    }
    
    public void setMaterial(String material) {
        this.material = material;
    }
    
    public double getPrice() {
        return this.price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getRawMaterial() {
        return this.rawMaterial;
    }
    
    public void setRawMaterial(String rawMaterial) {
        this.rawMaterial = rawMaterial;
    }
    
    public List<Container> getContainers() {
        return this.containers;
    }
    
    public void addContainer(Container container) {
        this.containers.add(container);
    }
    
    
    //Methods
    @Override
    public String toString() {
        return this.getCode() + " - " + this.getPrice();
    }
    
    @Override
    public boolean equals(Object obj) {
        Article ar = (Article) obj;
        return this.getCode().equals(ar.getCode());
    }
}
