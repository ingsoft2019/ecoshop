package classes;

import static com.teamdev.jxbrowser.engine.RenderingMode.HARDWARE_ACCELERATED;
import com.teamdev.jxbrowser.browser.Browser;
import com.teamdev.jxbrowser.engine.Engine;
import com.teamdev.jxbrowser.engine.EngineOptions;
import com.teamdev.jxbrowser.view.swing.BrowserView;
import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;


public class GoogleMaps {
    
    private List<SalePoint> salesPoints; 
    private final String mapUrl = "/others/map.html";
    private JFrame frame;
    private boolean isLoaded;
    
    public GoogleMaps() {
        salesPoints = new ArrayList<>();
        isLoaded = false;
    }
    
    //Getters & Setters
    public List<SalePoint> getSalesPoints() {
        return this.salesPoints;
    }
    
    public void setSalesPoints(List<SalePoint> sp) {
        this.salesPoints = sp;
    }
    
    public JFrame getFrame() {
        return this.frame;
    }
    
    public boolean isLoaded() {
        return this.isLoaded;
    }
    
    private void addSalesPointsToMap() {
        String points = "var locations = [";
        
        for (SalePoint sp : salesPoints) {
            points += "['" + sp.getTitle() 
                    + "', " + sp.getLatitude() 
                    + ", " + sp.getLength() 
                    + " ], \n";
        }
        
        points += "];";
        
        try {
            /** No funciono en .jar */
            //URI uri = GoogleMaps.class.getResource("/others/map.html").toURI(); 
            //final Map<String, String> env = new HashMap<>();
            //final String[] array = uri.toString().split("!");
            //final FileSystem fs = FileSystems.newFileSystem(URI.create(array[0]), env);
            //final Path path = fs.getPath(array[1]);
            //Charset charset = StandardCharsets.UTF_8;
            //byte[] bytes = Files.readAllBytes(path);
            //String content = new String(bytes, charset);
            //content = content.replace("var locations = [];", points);            
            //JOptionPane.showMessageDialog(null, content, "Error File Ex", JOptionPane.ERROR_MESSAGE);
            //Files.write(path, content.getBytes(charset));

            URI uri = new URI( GoogleMaps.class.getResource(mapUrl).toString() );
            Path path = Paths.get(uri);
            Charset charset = StandardCharsets.UTF_8;
            String content = new String(Files.readAllBytes(path), charset);
            content = content.replace("var locations = [];", points);
            Files.write(path, content.getBytes(charset));
            
        } catch (FileNotFoundException fileEx) {
            System.out.println("File Not Found!");
            JOptionPane.showMessageDialog(null, fileEx.getStackTrace(), "Error File Ex", JOptionPane.ERROR_MESSAGE);
        } catch (URISyntaxException uriEx) {
            System.out.println("URI Exception!");
            JOptionPane.showMessageDialog(null, uriEx.getStackTrace(), "Error URI Ex", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ioex) {
            System.out.println("IOException!");
            JOptionPane.showMessageDialog(null, ioex.getStackTrace(), "Error IO Ex", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace(), "Error E", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void createMap() {
        EngineOptions options;
        options = EngineOptions.newBuilder(HARDWARE_ACCELERATED)
                .licenseKey("6P830J66YACCIUWOUTYH3AWH422SEPT1HLISSK6XUYZ7RRZOUWOPG7HWCD2D6PZR6EZO ")
                .build();

        Engine engine = Engine.newInstance(options);

        Browser browser = engine.newBrowser();

        SwingUtilities.invokeLater(() -> {
            BrowserView view = BrowserView.newInstance(browser);

            frame = new JFrame("Google Maps");
            frame.add(view, BorderLayout.CENTER);
            frame.setSize(800, 500);
            frame.setVisible(false);
            frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
            addSalesPointsToMap();
            try {
                String url = GoogleMaps.class.getResource(mapUrl).toString();
                browser.navigation().loadUrl(url);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getStackTrace(), "Error E", JOptionPane.ERROR_MESSAGE);
            } finally {
                this.isLoaded = true;
            }

        });
    }
    
}
