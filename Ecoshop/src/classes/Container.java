package classes;

import java.io.Serializable;

public class Container implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private String name;
    private double price;
    
    public Container() {
        this.name = "";
        this.price = 0.0;
    }
    
    public Container(String name, double price) {
        this.name = name;
        this.price = price;
    }
    
    //Getters & Setters
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public double getPrice() {
        return this.price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
}
